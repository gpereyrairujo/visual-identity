#! /bin/bash

#for logo in $(ls *.svg);do
#  for size in 16 24 150; do
#    inkscape -z -e ${logo::-4}_$size.png -h $size -w $size $logo;
#  done;
#done
fout=public/index.html
mkdir public
cat Candidate_header.html > $fout

# skipping black and white add things to file
for logo in $(ls *.svg);do
  lg_name=${logo::-4}
  if [ "${lg_name:(-3)}" == "_bw" ]; then
    echo "skip $logo will do later"
  else
    echo "<h2>$lg_name</h2>" >> $fout
    
    for size in 150 24 16; do
      png=${lg_name}_$size.png
      inkscape -z -e public/$png -h $size -w $size $logo;
      echo "<img src=\"$png\">" >> $fout
      bw_ver=${lg_name}_bw.svg
      if [ -f "$bw_ver" ]; then
        png=${lg_name}_bw_$size.png
        inkscape -z -e public/$png -h $size -w $size ${lg_name}_bw.svg
        echo "<img src=\"$png\">" >> $fout
      fi
      echo " (width = ${size}px)</br>" >> $fout
    done
  fi
done