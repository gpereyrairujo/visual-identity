# GOSH Visual Identity

We are considering what GOSH should use as a logo. The most popular suggestion in the [forum](https://forum.openhardware.science/t/does-gosh-have-a-logo/2219) is something based on the [designs used for the roadmap](http://openhardware.science/global-open-science-hardware-roadmap/).

A good logo would look nice B&W so we can laser cut it or put it onto circuit boards. It is also best if it is recognisable when logo sized.

## This repository
This repository contains vectored versions of the images in SVG format. You can [see them displayed at different sizes here](https://gosh-community.gitlab.io/visual-identity/)

Please do:

* Add suggested logos in svg format to the repository (see below)
* Make an issue on the repository or message in the forum if you have an idea and don't know how to add a logo

## Adding a logo

* Put an SVG of the logo into the repository
* **Do not put spaces in the filename this will confuse the script**
* If you have a black and white version add _bw to the name

Once this is done GitLab will try to rebuild the website, this will take a couple of minutes.
